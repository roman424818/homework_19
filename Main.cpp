#include <iostream>


class Animal
{
private:
	std::string _type;
public:
	Animal() {}
	Animal(std::string type) : _type(type) {}
	virtual void Voice() = 0;
};

class Dog : public Animal
{
private:
	std::string _type;
public:
	Dog() {}
	Dog(std::string type) : _type(type) {}
	void Voice() override
	{
		std::cout << _type << ":\t" << "Woof!\n";
	}
};

class Cat : public Animal
{
private:
	std::string _type;
public:
	Cat() {}
	Cat(std::string type) : _type(type) {}
	void Voice() override
	{
		std::cout << _type << ":\t" << "Meow!\n";
	}
};

class Duck : public Animal
{
private:
	std::string _type;
public:
	Duck() {}
	Duck(std::string type) : _type(type) {}
	void Voice() override
	{
		std::cout << _type << ":\t" << "quack!\n";
	}
};

class Ping : public Animal
{
private:
	std::string _type;
public:
	Ping() {}
	Ping(std::string type) : _type(type) {}
	void Voice() override
	{
		std::cout << _type << ":\t" << "oink!\n";
	}
};

int main()
{
	Animal* arr[4];
	arr[0] = new Dog("Dog");
	arr[1] = new Cat("Cat");
	arr[2] = new Duck("Duck");
	arr[3] = new Ping("Ping");

	for (int i = 0; i < 4; i++)
	{
		arr[i]->Voice();
	}

}